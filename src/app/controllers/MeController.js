const Course = require('../model/course')
const {
    mongoseToObject,
    multipleMongoseToObject,
} = require('../../util/mongoose')

class MeController {
    //[GET] /me/courses
    courses(req, res, next) {
        Promise.all([
            Course.countDocumentsDeleted({}),
            Course.find({}).fnsort(req),
        ])
            .then(([countDeleted, courses]) => {
                res.render('me/courses', {
                    countDeleted,
                    courses: multipleMongoseToObject(courses),
                })
            })
            .catch(next)
    }

    //[GET] /me/:id/edit
    edit(req, res, next) {
        Course.findOne({ _id: req.params.id })
            .then((course) => {
                res.render('courses/update', {
                    course: mongoseToObject(course),
                })
            })
            .catch(next)
    }

    //[GET] /me/trashCourses
    trashCourses(req, res, next) {
        Course.findDeleted({})
            .then((courses) => {
                res.render('me/trashCourses', {
                    courses: multipleMongoseToObject(courses),
                })
            })
            .catch(next)
    }
}

module.exports = new MeController()
