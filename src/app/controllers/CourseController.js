const Course = require('../model/course')
const {
    mongoseToObject,
    multipleMongoseToObject,
} = require('../../util/mongoose')

class SiteController {
    // [GET] /courses
    course(req, res, next) {
        Course.find({})
            .then((courses) => {
                res.render('courses/course', {
                    courses: multipleMongoseToObject(courses),
                })
            })
            .catch(next)
    }

    // [GET] /courses/:slug
    detail(req, res, next) {
        let slug = req.params.slug
        Course.findOne({ slug })
            .then((course) => {
                res.render('courses/detail', {
                    course: mongoseToObject(course),
                })
            })
            .catch(next)
    }

    // [GET] /courses/create
    create(req, res, next) {
        res.render('courses/create')
    }

    // [POST] /courses/store
    store(req, res, next) {
        let formData = req.body
        formData.images = `https://i.ytimg.com/vi/${req.body.video}/mqdefault.jpg`
        let course = new Course(formData)
        course
            .save()
            .then(() => res.redirect('/me/courses'))
            .catch(next)
    }

    // [POST] /courses/submitCourse
    submitCourse(req, res, next) {
        switch (req.body.action) {
            case 'delete':
                Course.delete({ _id: req.body.checkedCourse })
                    .then(() => res.redirect('back'))
                    .catch(next)
                break
            default:
                res.json('error')
                break
        }
    }

    // [POST] /courses/submitTrash
    submitTrash(req, res, next) {
        switch (req.body.action) {
            case 'destroy':
                Course.deleteMany({ _id: req.body.checkedCourse })
                    .then(() => res.redirect('back'))
                    .catch(next)
                break
            case 'restore':
                Course.restore({ _id: req.body.checkedCourse })
                    .then(() => res.redirect('back'))
                    .catch(next)
                break
            default:
                res.json('error')
                break
        }
    }

    //[PUT] /courses/:id
    update(req, res, next) {
        Course.updateOne({ _id: req.params.id }, req.body)
            .then(() => res.redirect('/me/courses'))
            .catch(next)
    }

    //[DELETE] /courses/:id
    delete(req, res, next) {
        Course.delete({ _id: req.params.id })
            .then(() => res.redirect('back'))
            .catch(next)
    }

    //[DELETE] /courses/:id/destroy
    destroy(req, res, next) {
        Course.deleteOne({ _id: req.params.id })
            .then(() => res.redirect('back'))
            .catch(next)
    }

    //[PATCH] /courses/:id/restore
    restore(req, res, next) {
        Course.restore({ _id: req.params.id })
            .then(() => res.redirect('back'))
            .catch(next)
    }
}

module.exports = new SiteController()
