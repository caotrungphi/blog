class NewController {
    // [GET] /news
    index(req, res) {
        res.render('news')
    }

    // [GET] /news/:slug
    slug(req, res) {
        res.send('slug')
    }
}

module.exports = new NewController()
