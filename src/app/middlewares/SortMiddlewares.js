module.exports = function sort(req, res, next) {
    let initial = {
        type: 'default',
    }
    res.locals.sort = Object.assign(initial, req.query)
    next()
}
