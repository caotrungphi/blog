const mongoose = require('mongoose')
const slug = require('mongoose-slug-updater')
const mongooseDelete = require('mongoose-delete')
const AutoIncrement = require('mongoose-sequence')(mongoose)
const Schema = mongoose.Schema

const courses = new Schema(
    {
        _id: { type: Number },
        name: { type: String, required: true },
        description: { type: String },
        images: { type: String },
        video: { type: String },
        level: { type: String },
        slug: { type: String, slug: 'name', unique: true },
    },
    { _id: false, timestamps: true },
)

// customer method
courses.query.fnsort = function (req) {
    let valiType = ['desc', 'asc'].includes(req.query.type)
        ? req.query.type
        : 'desc'
    if (
        req.query.hasOwnProperty('colums') &&
        req.query.hasOwnProperty('type')
    ) {
        return this.sort({
            [req.query.colums]: valiType,
        })
    }
    return this
}

// library config
courses.plugin(AutoIncrement)
mongoose.plugin(slug)
courses.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true })

module.exports = mongoose.model('Coursess', courses)
