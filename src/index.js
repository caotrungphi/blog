const express = require('express')
const path = require('path')
const handlebars = require('express-handlebars')
var morgan = require('morgan')
const app = express()
const port = 3000
const route = require('./routes')
const db = require('./config/db/connet')
const methodOverride = require('method-override')
const sort = require('./app/middlewares/SortMiddlewares')
db.connect()
app.use(morgan('combined'))
app.use(methodOverride('_method'))

app.use(
    express.urlencoded({
        extended: true,
    }),
)
app.use(express.json())

app.use(express.static(path.join(__dirname, 'public')))

app.use(sort)

app.engine(
    'hbs',
    handlebars.engine({
        extname: '.hbs',
        helpers: require('./helpers/handlebars'),
    }),
)
app.set('view engine', 'hbs')
app.set('views', path.join(__dirname, 'resource', 'views'))

route(app)

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})
