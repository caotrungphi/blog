const Handlebars = require('handlebars')

module.exports = {
    inc: function (value, options) {
        return parseInt(value) + 1
    },
    fnsort: function (value, options) {
        let valuetype = value === options.colums ? options.type : 'default'
        let valiType = ['desc', 'asc', 'default'].includes(valuetype)
            ? valuetype
            : 'default'
        const icon = {
            default: 'chevron-expand-sharp',
            desc: 'chevron-down-outline',
            asc: 'chevron-up-outline',
        }
        const type = {
            default: 'desc',
            desc: 'asc',
            asc: 'desc',
        }

        let iconType = icon[valiType]
        let typeOptions = type[valiType]

        const validate = Handlebars.escapeExpression(
            `?colums=${value}&type=${typeOptions}`,
        )

        const result = `<a href="${validate}">
            <ion-icon name="${iconType}" class="icon"></ion-icon>
        </a>`
        return new Handlebars.SafeString(result)
    },
}
