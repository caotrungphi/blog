module.exports = {
    multipleMongoseToObject: function (courses) {
        return courses ? courses.map((course) => course.toObject()) : {}
    },
    mongoseToObject: function (course) {
        return course ? course.toObject() : {}
    },
}
