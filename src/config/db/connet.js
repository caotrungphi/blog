const mongoose = require('mongoose')

async function connect() {
    try {
        await mongoose.connect('mongodb://127.0.0.1:27017/blog_education_dev')
        console.log('connet succes !!!')
    } catch (error) {
        console.log('connet error !!!')
    }
}

module.exports = { connect }
