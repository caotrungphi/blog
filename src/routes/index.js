const news = require('./news')
const site = require('./site')
const course = require('./courses')
const me = require('./me')

function route(app) {
    app.use('/news', news)

    app.use('/courses', course)

    app.use('/me', me)

    app.use('/', site)
}

module.exports = route
