const express = require('express')
const router = express.Router()

const meController = require('../app/controllers/MeController')

router.get('/courses', meController.courses)

router.get('/trashCourses', meController.trashCourses)

router.get('/:id/edit', meController.edit)

module.exports = router
