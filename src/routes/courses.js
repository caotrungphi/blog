const express = require('express')
const router = express.Router()

const courseController = require('../app/controllers/CourseController')

router.get('/create', courseController.create)

router.post('/store', courseController.store)

router.post('/submitCourse', courseController.submitCourse)

router.post('/submitTrash', courseController.submitTrash)

router.put('/:id', courseController.update)

router.delete('/:id', courseController.delete)

router.delete('/:id/destroy', courseController.destroy)

router.patch('/:id/restore', courseController.restore)

router.get('/:slug', courseController.detail)

router.get('/', courseController.course)

module.exports = router
