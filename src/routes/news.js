const express = require('express')
const router = express.Router()

const newController = require('../app/controllers/NewController')

router.get('/:slug', (req, res) => newController[req.params.slug](req, res))

router.get('/', newController.index)

module.exports = router
